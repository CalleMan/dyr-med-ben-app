import { Component } from '@angular/core';

import {ModalController, NavController, NavParams} from 'ionic-angular';

import {FirestoreService} from "extended-angular-firebase";
import {ItemDetailsPage} from "../item-details/item-details";
import {FirestoreDocResult} from "extended-angular-firebase/src/app/modules/models/FirestoreDocResult";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  icons: string[];
  animals;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private exFirestore: FirestoreService,
      public modalCtrl: ModalController) {
    this.animals = this.exFirestore.collection('animals');
  }

  presentModal(data: FirestoreDocResult) {
    let modal = this.modalCtrl.create(ItemDetailsPage, {data: data});
    modal.present();
  }
}
