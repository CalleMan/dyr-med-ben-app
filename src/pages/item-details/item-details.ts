import { Component } from '@angular/core';

import {NavController, NavParams, ViewController} from 'ionic-angular';
import {AngularFirestore} from "angularfire2/firestore";
import {FirestoreDocResult} from "extended-angular-firebase/src/app/modules/models/FirestoreDocResult";

@Component({
  selector: 'page-item-details',
  templateUrl: 'item-details.html'
})
export class ItemDetailsPage {
  selectedItem: FirestoreDocResult;
  saving: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private firestore: AngularFirestore) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('data');
  }

  dismiss() {
      this.viewCtrl.dismiss();
  }

  saveData() {
      let promise: Promise<any>;
      this.saving = true;
      if (this.selectedItem.ref !== undefined) {
          promise = this.firestore.doc(this.selectedItem.ref.path).ref.set({
              name: this.selectedItem.data.name,
              legs: this.selectedItem.data.legs
          });
      } else {
          promise = this.firestore.collection('animals').add({
              name: this.selectedItem.data.name,
              legs: this.selectedItem.data.legs
          })
      }
      promise.then(data => {
          this.viewCtrl.dismiss();
      })
  }

  deleteAnimal() {
      this.saving = true;
      if (this.selectedItem.ref !== undefined) {
          this.firestore.doc(this.selectedItem.ref.path).ref.delete().then(data => {
              this.viewCtrl.dismiss()
          })
      }
  }
}
